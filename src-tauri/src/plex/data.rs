use serde::{Deserialize, Serialize};

// mediacontainer
#[derive(Debug, Serialize, Deserialize)]
pub struct ListLibraries {
  #[serde(rename(deserialize = "Directory"), default)]
  pub directories: Vec<Directory>,
}

// Rename to directory
#[derive(Debug, Serialize, Deserialize)]
pub struct Directory {
  pub key: String,
  pub title: String,
  pub thumb: Option<String>,

  #[serde(rename(deserialize = "type"), default)]
  pub directory_type: DirectoryType,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum DirectoryType {
  Movie,
  Show,
  Artist,
  Season,
  None,
}

impl Default for DirectoryType {
  fn default() -> Self {
    DirectoryType::None
  }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MovieLibrary {
  #[serde(rename(deserialize = "Video"), default)]
  pub videos: Vec<MovieOutline>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ShowLibrary {
  #[serde(rename(deserialize = "Directory"), default)]
  pub shows: Vec<Directory>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SubDirectoryList {
  pub art: String,
  pub banner: Option<String>,
  pub thumb: String,
  pub summary: Option<String>,

  #[serde(rename(deserialize = "Directory"), default)]
  pub subdirectories: Vec<Directory>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MovieOutline {
  pub key: String,
  pub title: String,
  pub thumb: String,
  pub year: u16,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Media {
  pub video_resolution: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MovieDetailContainer {
  #[serde(rename(deserialize = "Video"))]
  pub content: MovieDetail,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MovieDetail {
  pub key: String,
  pub title: String,
  pub thumb: String,
  pub art: String,

  pub summary: String,
  pub tagline: String,

  #[serde(default)]
  pub view_count: u16,
}

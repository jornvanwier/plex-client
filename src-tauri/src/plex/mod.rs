use anyhow::Context;
use reqwest::{blocking::Client, Url};
use serde::{Deserialize, Serialize};

pub mod data;

#[derive(Deserialize)]
pub struct PlexConfig {
  token: Option<String>,
  port: u16,
  host: String,
}

pub struct PlexAPI {
  config: PlexConfig,
}

impl PlexAPI {
  pub(crate) fn new(config: PlexConfig) -> Self {
    Self { config }
  }

  fn call<'a, T>(&self, path: &str) -> anyhow::Result<T>
  where
    T: Deserialize<'a> + Serialize,
  {
    self.call_with_params(path, &[])
  }

  fn call_with_params<'a, T>(&self, path: &str, params: &[(&str, &str)]) -> anyhow::Result<T>
  where
    T: Deserialize<'a> + Serialize,
  {
    let url = self.format_url(path, params)?;

    println!("REQUEST: {}", url);

    let client = Client::new();
    let response = client.get(url).send()?;

    let text = response.text()?;

    let parsed: T = serde_xml_rs::from_str(&text).context("Could not parse XML")?;
    Ok(parsed)
  }

  fn base_url(&self) -> anyhow::Result<Url> {
    let config = &self.config;
    let str_url = format!("http://{}:{}", config.host, config.port);

    Ok(Url::parse(&str_url)?)
  }

  fn add_auth(&self, url: &mut Url) {
    let config = &self.config;
    if let Some(token) = &config.token {
      url
        .query_pairs_mut()
        .extend_pairs([("X-Plex-Token", token)].iter());
    }
  }

  fn format_url(&self, path: &str, params: &[(&str, &str)]) -> anyhow::Result<Url> {
    let mut url = self.base_url()?.join(path)?;
    url.query_pairs_mut().extend_pairs(params.iter());
    self.add_auth(&mut url);
    Ok(url)
  }

  pub fn list_libraries(&self) -> anyhow::Result<data::ListLibraries> {
    self.call("library/sections")
  }

  pub fn retrieve_resource<'a, R>(&self, path: &str) -> anyhow::Result<R>
  where
    R: Serialize + Deserialize<'a>,
  {
    self.call(path)
  }

  pub fn get_transcode_url(&self, key: &str) -> anyhow::Result<String> {
    let offset = "0";
    // let copyts = "1";
    // let protocol = "hls";
    let media_index = "0";
    let platform = "Chrome";
    // let bitrate = "64";
    // let videoResolution

    let params = &[
      ("X-Plex-Platform", platform),
      // ("copyts", copyts),
      ("mediaIndex", media_index),
      ("offset", offset),
      ("path", key),
      ("directPlay", "0"),
      ("directStream", "1"),
      ("fastSeek", "1'"),
      // ("videoCodec", "h264")
      // ("fastSeek", "1"),
      // ("directStream","1")
      // ("protocol", protocol),
      // ("MaxVideoBitrate", bitrate),
    ];
    self
      .format_url("video/:/transcode/universal/start.m3u8", params)
      .map(Url::into_string)
  }
}

#![cfg_attr(
  all(not(debug_assertions), target_os = "windows"),
  windows_subsystem = "windows"
)]

use anyhow::anyhow;
use serde::{Deserialize, Serialize};
use tauri::Webview;

use crate::cmd::PlexCall;
use crate::plex::data::{
  DirectoryType, MovieDetailContainer, MovieLibrary, ShowLibrary, SubDirectoryList,
};
use crate::plex::PlexAPI;

mod cmd;
pub mod plex;

struct ExecuteContext<'a, 'b> {
  webview: &'a mut Webview<'b>,
  success_callback: String,
  error_callback: String,
}

impl<'a, 'b> ExecuteContext<'a, 'b> {
  pub fn new(
    webview: &'a mut Webview<'b>,
    success_callback: String,
    error_callback: String,
  ) -> Self {
    ExecuteContext {
      webview,
      success_callback,
      error_callback,
    }
  }

  pub fn execute<R, F>(self, f: F)
  where
    R: Serialize,
    F: FnOnce() -> anyhow::Result<R> + Send + 'static,
  {
    let handle_error = || f().map_err(|e| anyhow!("{:#}", e));
    tauri::execute_promise(
      self.webview,
      handle_error,
      self.success_callback,
      self.error_callback,
    )
  }
}

fn cmd_handler(webview: &mut Webview, arg: &str) -> Result<(), String> {
  let PlexCall {
    command,
    plex,
    callback,
    error,
  } = serde_json::from_str(arg).map_err(|e| e.to_string())?;

  let api = PlexAPI::new(plex);
  let ctx = ExecuteContext::new(webview, callback, error);

  use cmd::Cmd::*;
  match command {
    ListLibraries => ctx.execute(move || api.list_libraries()),
    LibraryContents { key, library_type } => ctx.execute(move || match library_type {
      DirectoryType::Movie => directory_contents::<MovieLibrary>(api, key, true),
      DirectoryType::Show => directory_contents::<ShowLibrary>(api, key, true),
      _ => unreachable!(),
    }),
    SubDirectoryContents { key, library_type } => ctx.execute(move || match library_type {
      DirectoryType::Season => directory_contents::<SubDirectoryList>(api, key, false),
      _ => unreachable!(),
    }),
    MovieDetail { key } => ctx.execute(move || api.retrieve_resource::<MovieDetailContainer>(&key)),
    GetTranscodeUrl { key } => ctx.execute(move || api.get_transcode_url(key.as_str())),
  };
  Ok(())
}

fn directory_contents<'a, R>(
  api: PlexAPI,
  key: String,
  library: bool,
) -> anyhow::Result<Box<dyn erased_serde::Serialize>>
where
  R: Serialize + Deserialize<'a> + 'static,
{
  let path = match library {
    true => format!("library/sections/{}/all", key),
    false => key,
  };

  let result = api.retrieve_resource::<R>(&path)?;
  let erased: Box<dyn erased_serde::Serialize> = Box::new(result);
  Ok(erased)
}

fn main() {
  tauri::AppBuilder::new()
    .invoke_handler(cmd_handler)
    .build()
    .run();
}

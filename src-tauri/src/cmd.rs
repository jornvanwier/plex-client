use crate::plex::data::DirectoryType;
use crate::plex::PlexConfig;
use serde::Deserialize;

#[derive(Deserialize)]
pub struct PlexCall {
  pub callback: String,
  pub plex: PlexConfig,
  pub error: String,
  #[serde(flatten)]
  pub command: Cmd,
}

#[derive(Deserialize)]
#[serde(tag = "cmd", rename_all = "camelCase")]
pub enum Cmd {
  // your custom commands
  // multiple arguments are allowed
  // note that rename_all = "camelCase": you need to use "myCustomCommand" on JS
  ListLibraries,
  LibraryContents {
    key: String,
    library_type: DirectoryType,
  },
  SubDirectoryContents {
    key: String,
    library_type: DirectoryType,
  },
  MovieDetail {
    key: String,
  },
  GetTranscodeUrl {
    key: String,
  },
}

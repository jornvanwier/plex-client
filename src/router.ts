import VueRouter from "vue-router";
import Vue from "vue";
import MovieLibrary from "@/components/views/library/MovieLibrary.vue";
import ShowLibrary from "@/components/views/library/ShowLibrary.vue";
import Show from "@/components/views/series/Show.vue";
import Movie from "@/components/views/Movie.vue";

Vue.use(VueRouter);

const routes = [
  {
    name: "moviesDirectory",
    path: "/library/movies/:libraryKey/",
    component: MovieLibrary,
    props: true
  },
  {
    name: "showsDirectory",
    path: "/library/shows/:libraryKey/",
    component: ShowLibrary,
    props: true
  },
  {
    name: "show",
    path: "/library/show/:showKey/",
    component: Show,
    props: true
  },
  { name: "movie", path: "/movie/:movieKey/", component: Movie, props: true }
];

const router = new VueRouter({
  routes
});

router.beforeEach((to, from, next) => {
  console.log(`Navigation from ${from.path} to ${to.path}`);

  if (to.path === '/') {
    console.log('Blocking navigation to /');
    return next(false);
  }

  next();
});

export default router;

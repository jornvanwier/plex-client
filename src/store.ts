import VuexPersistence from "vuex-persist";
import Vuex from "vuex";
import Vue from "vue";
import {PlexConfig} from "@/plex";

Vue.use(Vuex);

const persist = new VuexPersistence({
  storage: window.localStorage
});

export default new Vuex.Store({
  strict: true,
  state: {
    plex: {
      token: undefined,
      host: "localhost",
      port: 32400
    } as PlexConfig,
    darkMode: null as boolean | null,
  },
  mutations: {
    setToken: (state, token?: string) => (state.plex.token = token),
    setHost: (state, host: string) => (state.plex.host = host),
    setPort: (state, port: number) => (state.plex.port = port),
    setDarkMode: (state, value: boolean) => (state.darkMode  = value),
    RESTORE_MUTATION: persist.RESTORE_MUTATION,
  },
  plugins: [persist.plugin]
});

import { promisified } from "tauri/api/tauri";
import { Store } from "vuex";
import rootStore from "@/store";

export async function callRust(method: object) {
  try {
    return await promisified(method);
  } catch (e) {
    alert(e);
  }
}

export function vuexAccessor<T>(
    mutation: string,
    path: string,
    store: Store<{}> = rootStore
): { get(): T; set(v: T): void } {
  return {
    get(): T {
      return path
          .split(".")
          .reduce((obj, current) => obj[current], store.state as any);
    },
    set(value: T) {
      store.commit(mutation, value);
    }
  };
}

export default {
  callRust,
  vuexAccessor,
}
import * as data from "./plex-types";
import { MovieDetailContainer } from "./plex-types";
import utils from "@/utils";
import store from "@/store";

export interface PlexConfig {
  host: string;
  port: number;
  token?: string;
}

function plexConfig(): PlexConfig {
  return store.state.plex;
}

export default {
  plexURL(path = "/"): URL {
    const config = plexConfig();
    const base = `http://${config.host}:${config.port}`;
    const url = new URL(path, base);
    addTokenParam(url);
    return url;
  },

  async plexCommand(command: string, extra = {}): Promise<any> {
    console.log("Performing call", command, JSON.stringify(extra));
    return await utils.callRust({
      cmd: command,
      plex: plexConfig(),
      ...extra
    });
  },

  async listLibraries(): Promise<data.ListLibraries> {
    return await this.plexCommand("listLibraries");
  },

  async libraryContents(
    key: string,
    type: data.DirectoryType
  ): Promise<data.MovieLibrary | data.ShowLibrary> {
    return await this.plexCommand("libraryContents", {
      key: key,
      library_type: type
    });
  },

  async subDirectoryContents(
    key: string,
    type: data.DirectoryType
  ): Promise<data.SubDirectoryList> {
    return await this.plexCommand("subDirectoryContents", {
      key: key,
      library_type: type
    });
  },

  async movieDetail(key: string): Promise<MovieDetailContainer> {
    return await this.plexCommand("movieDetail", { key: key });
  },

  async getTranscodeUrl(key: string): Promise<string> {
      return await this.plexCommand("getTranscodeUrl", {key: key});
  },

  relativeUrlToAbsolute(relative: string): string | undefined {
    if (!relative) {
      return undefined;
    }

    let url = this.plexURL(relative);

    return url.toString();
  },

  transcodeImage(
    path: string,
    width: number,
    height: number
  ): string | undefined {
    if (!path) {
      return undefined;
    }

    const url = this.plexURL("/photo/:/transcode/");
    addParams(url, {
      width: width,
      height: height,
      url: path
    });

    return url.toString();
  },

  transcodeVideo(key: string): string {
    console.log('retrieving', key);

    const url = this.plexURL("/video/:/transcode/universal/start.m3u8");

    addParams(url, {
      protocol: 'hls',
      mediaIndex: 0,
      offset: 0,
      path: key,
      directPlay: 1,
      directStream: 1,
      fastSeek: 1,
      // TODO what platform is most suited?
      // "X-Plex-Platform": "Safari",
      "X-Plex-Platform": "Chrome",
    });

    return url.toString();
  }
};

function addTokenParam(url: URL) {
  const config = plexConfig();
  if (config.token) {
    addParams(url, { "X-Plex-Token": config.token });
  }
}

function addParams(url: URL, params: Object) {
  Object.entries(params).forEach(([key, value]) =>
    url.searchParams.append(key, value.toString())
  );
}
